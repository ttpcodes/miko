#!/bin/bash
set -e

input=$1
if [[ ${input:0:1} == '-' ]]; then
	operation=${input:1:1}
	flags=${input:2}
fi

case $operation in
	'S')
		# shellcheck source=/dev/null
		source PKGBUILD
		makepkg -si$flags
		;;
	*)
		;;
esac
